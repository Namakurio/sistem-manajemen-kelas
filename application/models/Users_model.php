<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model
{
    public function getUsers($level=null)
    {
        $this->datatables->select('id_user, nama_user,email_user,username_user,nama_level');
        $this->datatables->from('tbl_users');
        $this->datatables->join('tbl_level','tbl_users.id_level = tbl_level.id_level');
        if($level != null){
            $this->datatables->where('nama_level',$level);
        }
        $this->datatables->add_column('action', '
        <a href="javascript:void(0);" class="btn btn-small text-primary edit_users" data-id="$1" data-nama="$2" data-testimoni="$3">
            <i class="fas fa-edit"></i> Edit</a>
        <a href="javascript:void(0)" class="btn btn-small text-danger hapus_record" data-id="$1">
        <i class="fas fa-trash"></i> Hapus</a>',
        'id_user, nama_user, email_user');
        return $this->datatables->generate();
    }
}