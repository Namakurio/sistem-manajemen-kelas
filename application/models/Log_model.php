<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Log_model extends CI_Model
{
    function getLog() {
        $this->datatables->select('tbl_log.id_log as id,tbl_users.nama_user as nama, tbl_log.keterangan_log as keterangan, tbl_log.ip_log as ip, tbl_log.browser_log as browser, tbl_log.device_log as device, tbl_log.status_log as status');
        $this->datatables->from('tbl_log');
        $this->datatables->join('tbl_users','tbl_log.id_user = tbl_users.id_user');
        return $this->datatables->generate();
    }
}