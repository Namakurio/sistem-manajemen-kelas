<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Faq_model extends CI_Model
{
    public function getFaq() 
    {
        $this->datatables->select('id_pertanyaan, nama_pertanyaan, email_pertanyaan, judul_pertanyaan, pesan_pertanyaan');
        $this->datatables->from('tbl_pertanyaan');
        $this->datatables->add_column('lihat_jawaban', '
        <a href="'.site_url().'faq/list-jawaban/$1/" class="btn btn-small text-warning">
            <i class="fas fa-search"></i> Jawaban</a>',
        'id_pertanyaan');
        $this->datatables->add_column('action', '
        <a href="javascript:void(0);" class="btn btn-small text-primary jawab_pertanyaan" data-id="$1" data-nama="$2" data-email="$3"  data-judul="$4" data-faq="$5">
            <i class="fas fa-edit"></i> Jawab</a>
        <a href="javascript:void(0)" class="btn btn-small text-danger hapus_record" data-id="$1">
        <i class="fas fa-trash"></i> Hapus</a>',
        'id_pertanyaan, nama_pertanyaan, email_pertanyaan, judul_pertanyaan, pesan_pertanyaan');
        return $this->datatables->generate();
    }

    public function coutFaq($id_pertanyaan=null)
    {
        $this->db->select_sum('jawaban');
        $this->db->get('tbl_jawaban');
    }
    
    public function jawabFaq($id_pertanyaan)
    {
        $post = $this->input->post();
        $this->jawaban = $post['jawaban'];

        $dt_jawaban = array(
            'jawaban' => $this->jawaban,
            'id_user' => $this->session->userdata('id_user'),
            'id_pertanyaan' => $id_pertanyaan,
            'created_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('tbl_jawaban', $dt_jawaban);
        return array('status' => 'sukses', 'pesan' => 'Pertanyaan berhasil dijawab', 'icon' => '<i class="mdi mdi-check text-success"></i> ');
    }

    public function deleteFaq($id_pertanyaan)
    {
        $this->db->delete('tbl_pertanyaan', array("id_pertanyaan" => $id_pertanyaan));
        $this->db->delete('tbl_jawaban', array("id_pertanyaan" => $id_pertanyaan));
        return array('status' => 'sukses', 'pesan' => 'Pertanyaan berhasil dimasukkan', 'icon' => '<i class="mdi mdi-check text-success"></i> ');
    }

    public function getJawaban($id=null)
    {
        $this->datatables->select('tbl_jawaban.id_jawaban,tbl_jawaban.jawaban, tbl_users.nama_user,tbl_pertanyaan.judul_pertanyaan,tbl_pertanyaan.pesan_pertanyaan');
        $this->datatables->from('tbl_jawaban');
        $this->datatables->join('tbl_pertanyaan','tbl_jawaban.id_pertanyaan = tbl_pertanyaan.id_pertanyaan');
        $this->datatables->join('tbl_users','tbl_jawaban.id_user = tbl_users.id_user');
        if($id != null){
            $this->datatables->where('tbl_jawaban.id_pertanyaan',$id);
        }
        
        // $this->datatables->add_column('lihat_jawaban', '
        // <a href="'.site_url().'faq/list-jawaban/$1/" class="btn btn-small text-warning">
        //     <i class="fas fa-search"></i> Jawaban</a>',
        // 'id');
        $this->datatables->add_column('action', '
        <a href="javascript:void(0);" class="btn btn-small text-primary jawab_pertanyaan" data-id="$1" data-nama="$2" data-email="$3"  data-judul="$4" data-faq="$5">
            <i class="fas fa-edit"></i> Jawab</a>
        <a href="javascript:void(0)" class="btn btn-small text-danger hapus_record" data-id="$1">
        <i class="fas fa-trash"></i> Hapus</a>',
        'id_jawaban, jawaban, nama_user, judul_pertanyaan, pesan_pertanyaan');
        return $this->datatables->generate();
    }
}