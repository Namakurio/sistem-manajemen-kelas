<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Landing_model extends CI_Model
{
    //table
    private $_table_subscribe = "tbl_subscribe";
    private $_tbl_pertanyaan = "tbl_pertanyaan";
    //field user
    public $id_subscribe;
    public $email_subscribe;
    public $created_at;
    public $update_at;

    public function rules($option=null)
    {
        //rules
        if($option == "tambah-subscribe"){
            $rules = array(
                array('field' => 'email_subscribe', 'label' => 'Email', 'rules' => 'required'),);
        } else if($option == "tambah-pertanyaan"){
            $rules = array(
                array('field' => 'nama_pertanyaan', 'label' => 'Nama', 'rules' => 'required'),
                array('field' => 'email_pertanyaan', 'label' => 'Email', 'rules' => 'required'),
                array('field' => 'judul_pertanyaan', 'label' => 'Judul', 'rules' => 'required'),
                array('field' => 'pesan_pertanyaan', 'label' => 'Pesan', 'rules' => 'required'),);
        } else {
            $rules = array(
                array('field' => 'username_user', 'label' => 'Username', 'rules' => 'required'),
                array('field' => 'password_user', 'label' => 'Password', 'rules' => 'required'),);
        }

        return $rules;
    }

    public function addSubscribe()
    {
        //form post
        $post = $this->input->post();
        $this->email_subscribe = $post['email_subscribe'];
        //data
        $dt_subscribe = array(
            'email_subscribe' => $this->email_subscribe,
            'created_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s'),
        );
        //cek email
        $query = $this->db->get_where($this->_table_subscribe, ["email_subscribe" => $this->email_subscribe]);

        if($query->num_rows() == 0){
            // input subscribe
            $query = $this->db->insert($this->_table_subscribe,$dt_subscribe);    
            return array('status' => 'sukses', 'pesan' => 'email berhasil dimasukkan', 'icon' => '<i class="mdi mdi-check text-success"></i> ');
        } else {
            return array('status' => 'gagal', 'pesan' => 'email sudah terdaftar', 'icon' => '<i class="mdi mdi-close text-danger"></i> ');
        }
    }

    public function addAsk()
    {
        //form post
        $post = $this->input->post();
        $this->nama_pertanyaan = $post['nama_pertanyaan'];
        $this->email_pertanyaan = $post['email_pertanyaan'];
        $this->judul_pertanyaan = $post['judul_pertanyaan'];
        $this->pesan_pertanyaan = $post['pesan_pertanyaan'];
        //data
        $dt_pertanyaan = array(
            'nama_pertanyaan' => $this->nama_pertanyaan,
            'email_pertanyaan' => $this->email_pertanyaan,
            'judul_pertanyaan' => $this->judul_pertanyaan,
            'pesan_pertanyaan' => $this->pesan_pertanyaan,
            'created_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s'),
        );
        // input subscribe
        $query = $this->db->insert($this->_tbl_pertanyaan,$dt_pertanyaan);    
        return array('status' => 'sukses', 'pesan' => 'Pertanyaan berhasil dimasukkan', 'icon' => '<i class="mdi mdi-check text-success"></i> ');
    }
}