<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Testimoni_model extends CI_Model
{
    public function getTestimoni() 
    {
        $this->datatables->select('id_testimoni, nama_user,isi_testimoni');
        $this->datatables->from('tbl_testimoni');
        $this->datatables->join('tbl_users','tbl_testimoni.id_user = tbl_users.id_user');
        $this->datatables->add_column('action', '
        <a href="javascript:void(0);" class="btn btn-small text-success detail_testimoni" data-id="$1" data-nama="$2" data-testimoni="$3">
            <i class="fas fa-search"></i> Jawab</a>
        <a href="javascript:void(0)" class="btn btn-small text-danger hapus_record" data-id="$1">
        <i class="fas fa-trash"></i> Hapus</a>',
        'id_testimoni, nama_user, isi_testimoni');
        return $this->datatables->generate();
    }
}