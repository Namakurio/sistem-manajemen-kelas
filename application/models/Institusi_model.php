<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Institusi_model extends CI_Model
{
    public function getInstitusi($api=null)
    {
        $this->db->from('tbl_institusi');
        if($api == null){
            return $this->db->get()->result();
        } else {
            $result = array('status' => 'sukses', 'data' => $this->getKelas());
            return json_encode($result);
        }
    }

    public function getKelas($id_institusi=null)
    {
        $this->db->select('nama_kelas,nama_tingkatan,nama_institusi,alamat_institusi');        
        $this->db->from('tbl_kelas');
        $this->db->join('tbl_institusi','tbl_kelas.id_institusi=tbl_institusi.id_institusi');
        $this->db->join('tbl_tingkatan_kelas','tbl_kelas.id_tingkatan_kelas=tbl_tingkatan_kelas.id_tingkatan_kelas');
        if($id_institusi != null){
            $this->db->where('id_institusi',$id_institusi);
        }
        return $this->db->get()->result();
    }

    public function json($page_num=null)
    {
        
        $this->db->from('tbl_institusi');
        if($page_num != null){
            $set_limit = (($page_num - 1) * 2) . ",12";
            $this->db->limit($set_limit);   
        }

        $query = $this->db->get()->result();

        $result = array('status' => 'sukses', 'instisusi' => $query);
        return json_encode($result);
    }

    public function actual_row()
    {
        $result = $this->db->query("SELECT SQL_CALC_FOUND_ROWS * from tbl_institusi order by id_institusi asc limit 2");
        
        $row_object = $this->db->query("SELECT Found_Rows() as rowcount");
        $row_object = $row_object->row();
        return $row_object->rowcount;
    }
}