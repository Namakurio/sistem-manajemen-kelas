<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model
{
    //table
    private $_table_users = "tbl_users";
    private $_table_level = "tbl_level";
    //field user
    public $id_user;
    public $nama_user;
    public $email_user;
    public $username_user;
    public $password_user;
    public $id_level;
    //field level
    public $nama_level;

    public function rules()
    {
        //rules
        $rules = array(
            array('field' => 'username_user', 'label' => 'Username', 'rules' => 'required'),
            array('field' => 'password_user', 'label' => 'Password', 'rules' => 'required'),);

        return $rules;
    }

    public function login()
    {
        //form post
        $post = $this->input->post();
        $this->username_user = $post['username_user'];
        $this->password_user = $post['password_user'];
        
        //cek user
        $this->db->where('username_user',$this->username_user);
        $this->db->join($this->_table_level,'tbl_users.id_level = tbl_level.id_level');

        //query
        $query = $this->db->get($this->_table_users);
        $result = $query->row_array();

        if(password_verify($this->password_user, $result['password_user'])){
            return $result;
        } else {
            return array();
        }
    }
    
    public function cek_login($opsi){
        if($opsi == "login")
        {
            if($this->session->userdata('username_user'))
            {
                // $this->auth_model->cek_level($this->session->userdata('id_level'));
                redirect(site_url('dashboard'));
            }
        }
        else if($opsi == "masuk")
        {
            if(!$this->session->userdata('username_user')){
                redirect(site_url('login'));
            }
        }
    }

    public function role_dev($level){
        if($level != 'Developer'){
            redirect(site_url('forbidden'));
        }
        return;
    }

    public function role_admin($level){
        if($level != 'Admin'){
            redirect(site_url('forbidden'));
        }
        return;
    }
    
    public function role_operator($level){
        if($level != 'Operator'){
            redirect(site_url('forbidden'));
        }
        return;
    }
    
    public function role_ketua($level){
        if($level != 'Ketua Kelas'){
            redirect(site_url('forbidden'));
        }
        return;
    }
    public function role_wakil($level){
        if($level != 'Wakil Ketua Kelas'){
            redirect(site_url('forbidden'));
        }
        return;
    }
    public function role_sekretaris($level){
        if($level != 'Sekretaris'){
            redirect(site_url('forbidden'));
        }
        return;
    }
    public function role_bendahara($level){
        if($level != 'Bendahara'){
            redirect(site_url('forbidden'));
        }
        return;
    }
    public function role_anggota($level){
        if($level != 'Anggota'){
            redirect(site_url('forbidden'));
        }
        return;
    }

}