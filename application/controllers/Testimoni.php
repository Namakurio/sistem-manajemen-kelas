<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Testimoni extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("testimoni_model");
        $this->load->model("auth_model");
        $this->auth_model->cek_login("masuk");
    }

    public function index()
    {
        $this->load->view("dashboard/testimoni");
    }

    public function getTestimoni()
    {
        return print_r($this->testimoni_model->getTestimoni());
    }
}