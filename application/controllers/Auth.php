<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("auth_model");
	}
    
	public function index()
	{        
        //redirect halaman login
        redirect(site_url());
    }
    
    public function login()
	{        
        //cek login
        $this->auth_model->cek_login('login');
        //menampilkan halaman login
        $this->load->view("auth/login");
    }

    public function daftar()
	{        
        //cek login
        $this->auth_model->cek_login('login');
        //menampilkan halaman login
        $this->load->view("auth/daftar");
    }

    public function lupaPassword()
	{        
        //cek login
        $this->auth_model->cek_login('login');
        //menampilkan halaman login
        $this->load->view("auth/lupa-password");
    }

    public function proses()
    {
        //cek login
        $this->auth_model->cek_login('login');
        //inilisiasi model
        $auth = $this->auth_model;
        //iniliasi validation
        $validation = $this->form_validation;
        //rules
        $validation->set_rules($auth->rules());
        //validasi
        if($validation->run()){
            $login = $auth->login();

            //cek login
            if(!empty($login)){
                //buat session
                $this->session->set_userdata($login);
                //buat peringatan
                $this->session->set_flashdata('sukses','Selamat datang di halaman dashboard.');
                //masuk halaman user
                redirect(site_url('dashboard'));
            } else {
                //buat peringatan
                $this->session->set_flashdata('gagal','Username atau Password salah.');
                //kembali login
                redirect(site_url('login'));
            }
        }
        
    }

    public function logout()
    {
        //menghapus session
        $this->session->sess_destroy();
        //buat peringatan
        $this->session->set_flashdata('sukses','Anda berhasil logout.');
        //redirect
        redirect(site_url('login'));
    }

}