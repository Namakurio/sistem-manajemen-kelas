<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("users_model");
        $this->load->model("auth_model");
        $this->auth_model->cek_login("masuk");
    }

    public function index()
    {
        $this->load->view("dashboard/users/list");
    }

    public function getUsers($level=null)
    {
        return print_r($this->users_model->getUsers($level));
    }
}