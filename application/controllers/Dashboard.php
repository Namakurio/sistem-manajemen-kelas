<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("dashboard_model");
        $this->load->model("auth_model");
        $this->auth_model->cek_login("masuk");
    }

    public function index()
    {
        $this->load->view("dashboard/home");
    }
}