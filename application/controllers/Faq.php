<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        $this->load->model("faq_model");
        $this->load->model("auth_model");
        $this->auth_model->cek_login("masuk");
        $this->auth_model->role_dev($this->session->userdata('nama_level'));
    }

    public function index()
    {
        $this->load->view("dashboard/faq/list");
    }

    public function getFaq()
    {
        return print_r($this->faq_model->getFaq());
    }

    public function jawabFaq($id_faq=null)
    {
        if(!isset($id_faq)) {
            $result = array('status' => 'gagal', 'pesan' => 'ID belum dimasukkan', 'icon' => '<i class="mdi mdi-close text-danger"></i> ');
        }

        if($this->faq_model->jawabFaq($id_faq)){
           $result = array('status' => 'sukses', 'pesan' => 'Pertanyaan berhasil dijawab', 'icon' => '<i class="mdi mdi-check text-success"></i> ');
        }
        echo json_encode($result);
    }

    public function deleteFaq($id_faq=null)
    {
        if(!isset($id_faq)) {
            $result = array('status' => 'gagal', 'pesan' => 'ID belum dimasukkan', 'icon' => '<i class="mdi mdi-close text-danger"></i> ');
        }

        if($this->faq_model->deleteFaq($id_faq)){
           $result = array('status' => 'sukses', 'pesan' => 'Pertanyaan berhasil dimasukkan', 'icon' => '<i class="mdi mdi-check text-success"></i> ');
        }
        echo json_encode($result);
    }

    public function listJawaban($id=null)
    {        
        $data['id_get_jawaban'] = $id;
        $this->load->view("dashboard/faq/list-jawaban",$data);
    }

    public function getJawaban($id=null)
    {
        if($id==null){
            return print_r($this->faq_model->getJawaban());
        } else {
            return print_r($this->faq_model->getJawaban($id));
        }
        
    }

}