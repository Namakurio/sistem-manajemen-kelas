<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        $this->load->model("log_model");
        $this->load->model("auth_model");
        $this->auth_model->cek_login("masuk");
        $this->auth_model->role_dev($this->session->userdata('nama_level'));
    }

    public function index()
    {
        //menampilkan
        $this->load->view("dashboard/log");
    }

    function getLog(){
        header('Content-Type: application/json');
        return print_r($this->log_model->getLog());
    }
}