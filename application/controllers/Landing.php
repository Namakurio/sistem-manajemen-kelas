<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("landing_model");
    }

    public function index()
    {        
        $this->load->view("landing/main");
    }

    public function bantuan()
    {        
        $this->load->view("landing/main");
    }

    public function privasi()
    {        
        $this->load->view("landing/main");
    }

    public function ketentuan()
    {        
        $this->load->view("landing/main");
    }
    
    public function tambahSubScribe()
    {
        //inilisiasi model
        $landing = $this->landing_model;
        //iniliasi validation
        $validation = $this->form_validation;
        //rules
        $validation->set_rules($landing->rules('tambah-subscribe'));
        //validasi
        if($validation->run()){
            $addSubscribe = $landing->addSubscribe();
        }

        echo json_encode($addSubscribe);
    }

    public function tambahPertanyaan()
    {
        //inilisiasi model
        $landing = $this->landing_model;
        //iniliasi validation
        $validation = $this->form_validation;
        //rules
        $validation->set_rules($landing->rules('tambah-pertanyaan'));
        //validasi
        if($validation->run()){
            $addAsk = $landing->addAsk();
        }

        echo json_encode($addAsk);
    }
}