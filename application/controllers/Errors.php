<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Errors extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        // load view admin/overview.php
        $this->load->view("err/404");
    }

    public function err404()
    {
        $this->output->set_status_header('404');
        $this->load->view("err/404");	
    }

    public function err403()
    {
        $this->output->set_status_header('403');
        $this->load->view("err/403");	
    }

}