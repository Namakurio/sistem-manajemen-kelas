<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("profile_model");
        $this->load->model("auth_model");
        $this->auth_model->cek_login("masuk");     
    }

    public function index()
    {
        $this->load->view("dashboard/home");
    }
}