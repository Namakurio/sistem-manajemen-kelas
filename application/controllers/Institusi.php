<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Institusi extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("institusi_model");
        $this->load->model("auth_model");
        $this->auth_model->cek_login("masuk");
    }

    public function index()
    {
        $data['institusi'] = $this->institusi_model->getInstitusi();
        $data['actual_row_count'] = $this->institusi_model->actual_row();
        $this->load->view("dashboard/institusi/list",$data);
    }

    public function getInstitusi($page_num=null)
    {
        return print_r($this->institusi_model->json($page_num));
    }
    public function getCount()
    {
        return $this->institusi_model->actual_row();
    }
}