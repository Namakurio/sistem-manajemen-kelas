<!-- Vendor js -->
<script src="<?php echo base_url(); ?>assets/js/vendor.min.js"></script>

<!-- Plugins js-->
<script src="<?php echo base_url(); ?>assets/libs/jquery-countdown/jquery.countdown.min.js"></script>

<!-- Countdown js -->
<script src="<?php echo base_url(); ?>assets/js/pages/coming-soon.init.js"></script>

<!-- App js -->
<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>