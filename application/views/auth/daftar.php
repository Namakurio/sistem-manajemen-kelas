<!DOCTYPE html>
<html lang="en">    
<head>
    <?php $this->load->view('auth/_partials/head.php'); ?>
</head>
    <body class="authentication-bg">
        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <?php $this->load->view('auth/_partials/notif.php'); ?>
                        <div class="card bg-pattern">
                            <div class="card-body p-4">
                                <div class="text-center w-75 m-auto">
                                    <a href="<?php echo site_url(); ?>">
                                        <span><img src="<?php echo base_url(); ?>assets/images/logo-dark.png" alt="" height="22"></span>
                                    </a>
                                </div>
                                <form action="<?php echo site_url(); ?>proses/lupa-password" method="post">
                                    <div class="form-group mb-3">
                                        <label for="email_user">Email</label>
                                        <input class="form-control" type="email" name="email_user" id="email_user" required="" placeholder="Email Anda...">
                                    </div>
                                    <div class="form-group mb-0 text-center">
                                        <button class="btn btn-primary btn-block" type="submit"> Kirim </button>
                                    </div>
                                </form>
                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->

                        <div class="row mt-3">
                            <div class="col-12 text-center">
                                <p class="text-white-50">Sudah punya akun?  <a href="<?php echo site_url('login'); ?>" class="text-white"><b>Login</b></a></p>
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->

                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->
        <?php $this->load->view('auth/_partials/footer.php'); ?>
        <?php $this->load->view('auth/_partials/js.php'); ?>
    </body>
</html>