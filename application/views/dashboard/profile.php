<!DOCTYPE html>
<html lang="en">
<head>

    <?php $this->load->view('_partials/head.php'); ?>

</head>

<body>

    <?php $this->load->view('_partials/preloader.php'); ?>

    <!-- Begin page -->
    <div id="wrapper">

        <?php $this->load->view('_partials/topbar.php'); ?>

        <?php $this->load->view('_partials/navbar.php'); ?>

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">

                    <?php $this->load->view('_partials/breadcrumb.php'); ?>

                    <?php $this->load->view('_partials/notif.php'); ?>
                    
                    <div class="row">
                        <div class="col-md-6 col-xl-3">
                            <div class="widget-rounded-circle card-box">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="avatar-lg rounded bg-soft-primary">
                                            <i class="dripicons-graph-bar font-24 avatar-title text-primary"></i>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="text-right">
                                            <h3 class="text-dark mt-1">Rp.<span data-plugin="counterup"><?php echo number_format($pemasukan_saya,0,".",","); ?></span></h3>
                                            <p class="text-muted mb-1 text-truncate">Pemasukan</p>
                                        </div>
                                    </div>
                                </div> <!-- end row-->
                            </div> <!-- end widget-rounded-circle-->
                        </div> <!-- end col-->

                        <div class="col-md-6 col-xl-3">
                            <div class="widget-rounded-circle card-box">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="avatar-lg rounded bg-soft-success">
                                            <i class="dripicons-graph-line font-24 avatar-title text-success"></i>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="text-right">
                                            <h3 class="text-dark mt-1">Rp.<span data-plugin="counterup"><?php echo number_format($pengeluaran_saya,0,".",","); ?></span></h3>
                                            <p class="text-muted mb-1 text-truncate">Pengeluaran</p>
                                        </div>
                                    </div>
                                </div> <!-- end row-->
                            </div> <!-- end widget-rounded-circle-->
                        </div> <!-- end col-->

                        <div class="col-md-6 col-xl-3">
                            <div class="widget-rounded-circle card-box">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="avatar-lg rounded bg-soft-info">
                                            <i class="dripicons-card font-24 avatar-title text-info"></i>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="text-right">
                                            <h3 class="text-dark mt-1">Rp.<span data-plugin="counterup"><?php echo number_format($tabungan_saya,0,".",","); ?></span></h3>
                                            <p class="text-muted mb-1 text-truncate">Saldo</p>
                                        </div>
                                    </div>
                                </div> <!-- end row-->
                            </div> <!-- end widget-rounded-circle-->
                        </div> <!-- end col-->

                        <div class="col-md-6 col-xl-3">
                            <div class="widget-rounded-circle card-box">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="avatar-lg rounded bg-soft-warning">
                                            <i class="dripicons-swap font-24 avatar-title text-warning"></i>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="text-right">
                                            <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo $transaksi_saya; ?></span></h3>
                                            <p class="text-muted mb-1 text-truncate">Total Transaksi</p>
                                        </div>
                                    </div>
                                </div> <!-- end row-->
                            </div> <!-- end widget-rounded-circle-->
                        </div> <!-- end col-->
                    </div>
                    <!-- end row -->
                    <?php if($this->session->userdata('nama_level') == "Developer" || $this->session->userdata('nama_level') == "Admin"): ?>
                    <div class="row">
                        <div class="col-12">
                            <div class="card-box widget-inline">
                                <div class="row">
                                    <div class="col-sm-6 col-xl-3">
                                        <div class="p-2 text-center">
                                            <i class="mdi mdi-bank-transfer text-primary mdi-24px"></i>
                                            <h3><span data-plugin="counterup"><?php echo $count_transaksi; ?></span></h3>
                                            <p class="text-muted font-15 mb-0">Transaksi</p>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-xl-3">
                                        <div class="p-2 text-center">
                                            <i class="mdi mdi-bank text-success mdi-24px"></i>
                                            <h3>Rp.<span data-plugin="counterup"><?php echo number_format($total_uang,0,".",","); ?></span></h3>
                                            <p class="text-muted font-15 mb-0">Total Uang</p>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-xl-3">
                                        <div class="p-2 text-center">
                                            <i class="mdi mdi-account-group text-danger mdi-24px"></i>
                                            <h3><span data-plugin="counterup"><?php echo $count_nasabah; ?></span></h3>
                                            <p class="text-muted font-15 mb-0">Total Nasabah</p>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-xl-3">
                                        <div class="p-2 text-center">
                                            <i class="mdi mdi-account-supervisor-circle text-blue mdi-24px"></i>
                                            <h3><span data-plugin="counterup"><?php echo $count_user; ?></span></h3>
                                            <p class="text-muted font-15 mb-0">Total User</p>
                                        </div>
                                    </div>

                                </div> <!-- end row -->
                            </div> <!-- end card-box-->
                        </div> <!-- end col-->
                    </div>    
                    <!-- end row-->
                    
                    <div class="row">
                        <div class="col-xl-5">
                            <div class="card-box">
                                <h4 class="header-title mb-3">User Baru</h4>

                                <div class="table-responsive">
                                    <table class="table table-centered table-hover mb-0">
                                        <thead>
                                            <tr>
                                                <th class="border-top-0">Nama</th>
                                                <th class="border-top-0">Username</th>
                                                <th class="border-top-0">Email</th>
                                                <th class="border-top-0">Level</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($recent_user as $rcu): ?>
                                            <tr>
                                                <td><?php echo $rcu->nama_user ?></td>
                                                <td><?php echo $rcu->username_user ?></td>
                                                <td><?php echo $rcu->email_user ?></td>
                                                <td>
                                                    <span class="badge <?php if($rcu->nama_level == "Administrator"){ echo "badge-danger"; }else if($rcu->nama_level == "Operator"){ echo "badge-warning";}else if($rcu->nama_level == "Peminjam"){ echo "badge-primary";}?>"><?php echo $rcu->nama_level ?></span>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        
                                        </tbody>
                                    </table>
                                </div> <!-- end table-responsive -->

                            </div> <!-- end card-box-->
                        </div> <!-- end col-->
                        <div class="col-xl-7">
                            <div class="card-box">
                                <h4 class="header-title mb-3">Barang Baru</h4>

                                <div class="table-responsive">
                                    <table class="table table-centered table-hover mb-0">
                                        <thead>
                                            <tr>
                                                <th class="border-top-0">Nama</th>
                                                <th class="border-top-0">Kondisi</th>
                                                <th class="border-top-0">Jumlah</th>
                                                <th class="border-top-0">Jenis</th>
                                                <th class="border-top-0">Tanggal Ditambahkan</th>
                                                <th class="border-top-0">Ruangan</th>
                                                <th class="border-top-0">Petugas</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($recent_barang as $rcb): ?>
                                            <tr>
                                                <td><?php echo $rcb->nama_inventaris; ?></td>
                                                <td><?php echo $rcb->kondisi_inventaris; ?></td>
                                                <td><?php echo $rcb->jumlah_inventaris; ?></td>
                                                <td><?php echo $rcb->nama_jenis; ?></td>
                                                <td><?php echo $rcb->tanggal_register; ?></td>
                                                <td><?php echo $rcb->nama_ruang; ?></td>
                                                <td><?php echo $rcb->nama_user; ?></td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div> <!-- end table-responsive -->
                            </div> <!-- end card-box-->
                        </div> <!-- end col-->
                    </div>
                    <!-- end row-->
                    <!-- end row -->
                    <div class="row">
                        <div class="col-12">
                            <div class="card-box">
                                <h4 class="header-title mb-3">Barang Dipinjam</h4>

                                <div class="table-responsive">
                                    <table class="table table-centered table-hover mb-0">
                                        <thead>
                                            <tr>
                                                <th class="border-top-0">Barang</th>
                                                <th class="border-top-0">Tanggal Pinjam</th>
                                                <th class="border-top-0">Tanggal Kembali</th>
                                                <th class="border-top-0">Peminjam</th>
                                                <th class="border-top-0">Jumlah Pinjaman</th>
                                                <th class="border-top-0">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($recent_pinjam as $rcp): ?>
                                            <tr>
                                                <td><?php echo $rcp->id_peminjaman; ?></td>
                                                <td><?php echo $rcp->tanggal_pinjam; ?></td>
                                                <td><?php echo $rcp->tanggal_kembali; ?></td>
                                                <td><?php echo $rcp->nama_user; ?></td>
                                                <td><?php echo $rcp->nama_user; ?></td>
                                                <td><span class="badge bg-soft-success text-success shadow-none"><?php echo $rcp->status_peminjaman ?></span></td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div> <!-- end table-responsive -->
                            </div> <!-- end card-box-->
                        </div> <!-- end col-->
                    </div>
                    <!-- end row-->
                    <?php endif; ?>

                </div> <!-- container -->

            </div> <!-- content -->

            <?php $this->load->view('_partials/footer.php'); ?>

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->

    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>

    <?php $this->load->view('_partials/modal.php'); ?>
    
    <?php $this->load->view('_partials/js.php'); ?>

</body>
</html>