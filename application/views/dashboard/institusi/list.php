<!DOCTYPE html>
<html lang="en">
<head>

    <?php $this->load->view('_partials/head.php'); ?>

</head>

<body>

    <?php $this->load->view('_partials/preloader.php'); ?>

    <!-- Begin page -->
    <div id="wrapper">

        <?php $this->load->view('_partials/topbar.php'); ?>

        <?php $this->load->view('_partials/navbar.php'); ?>

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">

                    <?php $this->load->view('_partials/breadcrumb.php'); ?>

                    <?php $this->load->view('_partials/notif.php'); ?>
                    
                    <div class="row mb-2">
                        <div class="col-sm-4">
                            <button type="button" class="btn btn-danger btn-rounded mb-3"><i class="mdi mdi-plus"></i> Tambah Sekolah</button>
                        </div>
                        <div class="col-sm-8">
                            <div class="text-sm-right">
                                <div class="btn-group mb-3">
                                    <button type="button" class="btn btn-primary">Semua</button>
                                </div>
                                <div class="btn-group mb-3 ml-1">
                                    <button type="button" class="btn btn-light">Unverified</button>
                                    <button type="button" class="btn btn-light">Verified</button>
                                </div>
                                <div class="btn-group mb-3 ml-2 d-none d-sm-inline-block">
                                    <button type="button" class="btn btn-dark"><i class="mdi mdi-apps"></i></button>
                                </div>
                                <div class="btn-group mb-3 d-none d-sm-inline-block">
                                    <button type="button" class="btn btn-link text-dark"><i class="mdi mdi-format-list-bulleted-type"></i></button>
                                </div>
                            </div>
                        </div><!-- end col-->
                    </div> 
                    <!-- end row-->


                    <div class="row" id="main_institusi">
                        <main></main>
                    </div>
                    <!-- end row -->

                    <div class="row" id="main_load_more">
                        <div class="col-12">
                            <div class="text-center mb-3">
                                <a href="javascript:void(0);" class="text-danger"><i class="mdi mdi-spin mdi-loading mr-1"></i> Sedang memuat </a>
                            </div>
                        </div> <!-- end col-->
                    </div>
                    <!-- end row -->

                </div> <!-- container -->

            </div> <!-- content -->

            <?php $this->load->view('_partials/footer.php'); ?>

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->

    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>

    <?php $this->load->view('_partials/modal.php'); ?>
    
    <?php $this->load->view('_partials/js.php'); ?>
    <script type="text/javascript" charset="utf-8" async defer>

    const main_institusi = document.querySelector('#main_institusi');
    const main_load_more = document.getElementById('main_load_more');
    const defaultSource = 2;

    window.addEventListener('scroll', async e => {

        // console.log($(window).scrollTop() + $(window).height() == $(document).height());

    });
    
    if($(window).scrollTop() + $(window).height() > $(document).height() - 200) {
        main_load_more.style.display = 'block';
    }
    if($(window).scrollTop() + $(window).height() == $(document).height()) {

        main_load_more.style.display = 'hide';

        var page = 1;

        window.addEventListener('scroll', async e => {

            page++;
            var data = {
                page_num: page
            };

            var actual = "<?php echo $actual_row_count; ?>";
            console.log($(window).scrollTop() + $(window).height() == $(document).height());
            if((page-1)* 2 > actual){
                main_load_more.style.display = 'block';
            }else{
                updateNews();      
                console.log(data);          
            }
            
        });
        
        window.addEventListener('load', async e => {

            updateNews();

        });
        

        async function updateNews(){

            const res = await fetch(`<?php echo site_url('institusi/getInstitusi/'); ?>`+page);
            const json = await res.json();

            main_institusi.innerHTML = json.instisusi.map(createInstitusi).join('\n');

        }

        function createInstitusi(instisusi){

            return `
                <div class="col-xl-4">
                    <div class="card-box project-box">
                        <div class="dropdown float-right">
                            <a href="#" class="dropdown-toggle card-drop arrow-none" data-toggle="dropdown" aria-expanded="false">
                                <i class="mdi mdi-dots-horizontal m-0 text-muted h3"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Detail</a>
                                <a class="dropdown-item" href="#">Edit</a>
                                <a class="dropdown-item" href="#">Delete</a>
                                <a class="dropdown-item" href="#">Tambah Kelas</a>
                            </div>
                        </div> <!-- end dropdown -->
                        <!-- Title-->
                        <h4 class="mt-0"><a href="javascript: void(0);" class="text-dark"></a></h4>
                        <p class="text-muted text-uppercase">
                            <!-- <i class="mdi mdi-account-circle"></i> <small>Rio Prastiawan</small> -->
                            <i class="mdi mdi-phone-classic"></i> <small>08990125448</small>
                        </p>
                        <div class="badge bg-soft-success text-success mb-3 shadow-none">Verified</div>
                        <!-- Desc-->
                        <p class="text-muted font-13 mb-3 sp-line-2">SMKNNNNN<a href="javascript:void(0);" class="text-muted"></a>
                        </p>
                        <!-- Task info-->
                        <p class="mb-1">
                            <span class="pr-2 text-nowrap mb-2 d-inline-block">
                                <i class="mdi mdi-door-open text-muted"></i>
                                <b>100</b> Kelas
                            </span>
                            <span class="text-nowrap mb-2 d-inline-block">
                                <i class="mdi mdi-account-supervisor-circle text-muted"></i>
                                <b>100</b> Siswa
                            </span>
                        </p>
                        <!-- Team-->
                        <div class="avatar-group mb-3">
                            <a href="javascript: void(0);" class="avatar-group-item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mat Helme">
                                <img src="assets/images/users/user-1.jpg" class="rounded-circle avatar-sm" alt="friend" />
                            </a>
                        </div>
                        <!-- Progress-->
                        <p class="mb-2">Task completed: <span class="float-right">28/78</span></p>
                        <div class="progress mb-1" style="height: 7px;">
                            <div class="progress-bar"
                                    role="progressbar" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100"
                                    style="width: 34%;">
                            </div><!-- /.progress-bar .progress-bar-danger -->
                        </div><!-- /.progress .no-rounded -->

                    </div> <!-- end card box-->
                </div><!-- end col-->
            `;

        }
    }
    
    </script>
</body>
</html>