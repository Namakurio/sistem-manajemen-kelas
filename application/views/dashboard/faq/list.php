<!DOCTYPE html>
<html lang="en">
<head>

    <?php $this->load->view('_partials/head.php'); ?>

</head>

<body>

    <?php $this->load->view('_partials/preloader.php'); ?>

    <!-- Begin page -->
    <div id="wrapper">

        <?php $this->load->view('_partials/topbar.php'); ?>

        <?php $this->load->view('_partials/navbar.php'); ?>

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">

                    <?php $this->load->view('_partials/breadcrumb.php'); ?>

                    <?php $this->load->view('_partials/notif.php'); ?>
                    
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="header-title mb-4">Data Testimoni</h4>

                                    <table id="datatables-faq" class="table table-hover m-0 table-centered dt-responsive nowrap w-100">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>Judul</th>
                                                <th>Pesan</th>
                                                <th>Lihat Jawaban</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>Judul</th>
                                                <th>Pesan</th>
                                                <th>Lihat Jawaban</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>

                                </div> <!-- end card body-->
                            </div> <!-- end card -->
                        </div><!-- end col-->
                    </div>
                    <!-- end row-->

                </div> <!-- container -->

            </div> <!-- content -->

            <?php $this->load->view('_partials/footer.php'); ?>

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->

    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>

    <?php $this->load->view('_partials/modal.php'); ?>
    
    <?php $this->load->view('_partials/js.php'); ?>

    <script type="text/javascript">
            $(document).ready(function() {
                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };
 
                var t = $("#datatables-faq").dataTable({
                    initComplete: function() {
                        var api = this.api();
                        $('#mytable_filter input')
                                .off('.DT')
                                .on('keyup.DT', function(e) {
                                    if (e.keyCode == 13) {
                                        api.search(this.value).draw();
                            }
                        });
                    },
                    processing: true,
                    serverSide: true,
                    ajax: {"url": "<?php echo site_url('faq/getfaq'); ?>", "type": "POST"},
                    columns: [
                        {
                            "data": "id_pertanyaan",
                            "orderable": false, width:10
                        },
                        {"data": "nama_pertanyaan", width:10},
                        {"data": "email_pertanyaan", width:10},
                        {"data": "judul_pertanyaan", width:10},
                        {"data": "pesan_pertanyaan", width:10},
                        {"data": "lihat_jawaban", width:10},
                        {"data": "action", width:10},
                    ],
                    order: [[1, 'asc']],
                    rowCallback: function(row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    },
                    keys:!0,
                    language:{
                        paginate:{
                            previous:"<i class='mdi mdi-chevron-left'>",
                            next:"<i class='mdi mdi-chevron-right'>"
                        },
                        sProcessing: "loading..."
                    },
                    drawCallback:function(){
                        $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
                    },
                    stateSave: true
                });
                // get Edit Records
                $('#datatables-faq').on('click','.jawab_pertanyaan',function(){

                    var id=$(this).data('id');
                    var nama=$(this).data('nama');
                    var email=$(this).data('email');
                    var judul=$(this).data('judul');
                    var faq=$(this).data('faq');

                    $('#jawabPertanyaan').modal('toggle');

                    $('[name="nama_faq"]').val(nama);
                    $('[name="email_faq"]').val(email);
                    $('[name="judul_faq"]').val(judul);
                    $('[name="soal_faq"]').val(faq);

                    $('#kirim_jawaban').on('click',function(){

                        var jawaban = $('[name="jawaban_faq"]').val();

                        $.ajax({
                            type:'POST',
                            data: 'jawaban='+jawaban,
                            url: 'faq/jawabFaq/'+id,
                            success: function(result){
                                var response = JSON.parse(result);
                                //tutup modal
                                $('#jawabPertanyaan').modal('toggle');
                            }
                        })
                    })

                });
                // End Edit Records
                // get Hapus Records
                $('#datatables-faq').on('click','.hapus_record',function(){
                    
                    var id =$(this).data('id');

                    $('#deleteModal').modal('toggle');
                    $('#btn-delete').on('click',function(){

                        $.ajax({
                            url: 'faq/deleteFaq/'+id,
                            success: function(result){
                                var response = JSON.parse(result);
                                //tutup modal
                                $('#deleteModal').modal('toggle');
                            }
                        })

                    });
                });
                // End Hapus Records
            });
        </script>
</body>
</html>