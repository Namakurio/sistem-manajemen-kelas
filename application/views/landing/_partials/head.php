<meta charset="utf-8" />
        <title>UBold - Responsive Admin Dashboard & Landing Page Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?php echo site_url(); ?>assets_landing/images/favicon.ico">

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo site_url(); ?>assets_landing/css/bootstrap.min.css" type="text/css">

        <!--Material Icon -->
        <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets_landing/css/materialdesignicons.min.css" />

        <!-- Custom  sCss -->
        <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets_landing/css/style.css" />