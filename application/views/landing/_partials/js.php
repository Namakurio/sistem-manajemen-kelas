<!-- javascript -->
<script src="<?php echo site_url(); ?>assets_landing/js/jquery.min.js"></script>
<script src="<?php echo site_url(); ?>assets_landing/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo site_url(); ?>assets_landing/js/jquery.easing.min.js"></script>
<script src="<?php echo site_url(); ?>assets_landing/js/scrollspy.min.js"></script>

<!-- custom js -->
<script src="<?php echo site_url(); ?>assets_landing/js/app.js"></script>