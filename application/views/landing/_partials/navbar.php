<!--Navbar Start-->
<nav class="navbar navbar-expand-lg fixed-top navbar-custom sticky sticky-dark">
        <div class="container-fluid">
            <!-- LOGO -->
            <a class="logo text-uppercase" href="index-2.html">
                <img src="images/logo-light.png" alt="" class="logo-light" height="18" />
                <img src="images/logo-dark.png" alt="" class="logo-dark" height="18" />
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="mdi mdi-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mx-auto navbar-center" id="mySidenav">
                    <li class="nav-item active">
                        <a href="#home" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="#fitur" class="nav-link">Fitur</a>
                    </li>
                    <li class="nav-item">
                        <a href="#demo" class="nav-link">Demos</a>
                    </li>
                    <li class="nav-item">
                        <a href="#pricing" class="nav-link">Pricing</a>
                    </li>
                    <li class="nav-item">
                        <a href="#pertanyaan" class="nav-link">Pertanyaan</a>
                    </li>
                    <li class="nav-item">
                        <a href="#testimoni" class="nav-link">Testimoni</a>
                    </li>
                    <li class="nav-item">
                        <a href="#kontak" class="nav-link">Kontak</a>
                    </li>
                </ul>
                <a href="<?php echo site_url('daftar'); ?>" class="btn btn-outline-info navbar-btn mr-1">Daftar</a>
                <a href="<?php echo site_url('login'); ?>" class="btn btn-warning navbar-btn">Masuk</a>
            </div>
        </div>
    </nav>
    <!-- Navbar End -->