<!-- footer start -->
<footer class="bg-dark footer">
        <div class="container-fluid">
            <div class="row mb-5">
                <div class="col-lg-4">
                    <div class="pr-lg-4">
                        <div class="mb-4">
                            <img src="images/logo-light.png" alt="" height="20">
                        </div>
                        <p class="text-white-50">Sistem Manajemen Kelas</p>
                        <p class="text-white-50">Memudahkan anda dalam memanage kelas dengan lebih rinci dan meminimalisir kesalahan data.</p>
                    </div>
                </div>
                
                <div class="col-lg-2">
                    <div class="footer-list">
                        <p class="text-white mb-2 footer-list-title">Tentang</p>
                        <ul class="list-unstyled">
                            <li><a href="#home"><i class="mdi mdi-chevron-right mr-2"></i>Home</a></li>
                            <li><a href="#fitur"><i class="mdi mdi-chevron-right mr-2"></i>Features</a></li>
                            <li><a href="#pertanyaan"><i class="mdi mdi-chevron-right mr-2"></i>Pertanyaan</a></li>
                            <li><a href="#testimoni"><i class="mdi mdi-chevron-right mr-2"></i>Testimoni</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-2">
                    <div class="footer-list">
                        <p class="text-white mb-2 footer-list-title">Sosial Media</p>
                        <ul class="list-unstyled">
                            <li><a href="http://facebook.com/prastiawanrioo" target="_blank"><i class="mdi mdi-chevron-right mr-2"></i>Facebook </a></li>
                            <li><a href="https://twitter.com/prastiawanrio" target="_blank"><i class="mdi mdi-chevron-right mr-2"></i>Twitter</a></li>
                            <li><a href="https://instagram.com/rioprastiawan" target="_blank"><i class="mdi mdi-chevron-right mr-2"></i>Instagram</a></li>
                            <li><a href="https://google.com" target="_blank"><i class="mdi mdi-chevron-right mr-2"></i>Google+</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-2">
                    <div class="footer-list">
                        <p class="text-white mb-2 footer-list-title">Dukungan</p>
                        <ul class="list-unstyled">
                            <li><a href="<?php echo site_url('bantuan'); ?>"><i class="mdi mdi-chevron-right mr-2"></i>Bantuan & Dukungan</a></li>
                            <li><a href="<?php echo site_url('privasi'); ?>"><i class="mdi mdi-chevron-right mr-2"></i>Kebijakan Privasi</a></li>
                            <li><a href="<?php echo site_url('ketentuan'); ?>"><i class="mdi mdi-chevron-right mr-2"></i>Istilah & Ketentuan</a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-lg-2">
                    <div class="footer-list">
                        <p class="text-white mb-2 footer-list-title">Info Lebih Lanjut</p>
                        <ul class="list-unstyled">
                            <li><a href="#"><i class="mdi mdi-chevron-right mr-2"></i>Pricing</a></li>
                            <li><a href="#"><i class="mdi mdi-chevron-right mr-2"></i>For Marketing</a></li>
                            <li><a href="#"><i class="mdi mdi-chevron-right mr-2"></i>For Agencies</a></li>
                            <li><a href="#"><i class="mdi mdi-chevron-right mr-2"></i>Our Apps</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-lg-12">
                    <div class="float-left pull-none">
                        <p class="text-white-50">Rio Prastiawan <i class="mdi mdi-heart-outline text-danger"></i> All Right Reserved.</p>
                    </div>
                    <div class="float-right pull-none">
                        <ul class="list-inline social-links">
                            <li class="list-inline-item text-white-50">
                                Sosial Media :
                            </li>
                            <li class="list-inline-item"><a href="http://facebook.com/prastiawanrioo" target="_blank"><i class="mdi mdi-facebook"></i></a></li>
                            <li class="list-inline-item"><a href="https://twitter.com/prastiawanrio" target="_blank"><i class="mdi mdi-twitter"></i></a></li>
                            <li class="list-inline-item"><a href="https://instagram.com/rioprastiawan" target="_blank"><i class="mdi mdi-instagram"></i></a></li>
                            <li class="list-inline-item"><a href="https://google.com" target="_blank"><i class="mdi mdi-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- container-fluid -->
    </footer>
    <!-- footer end -->