<!DOCTYPE html>
<html lang="en">    
<head>
    
    <?php $this->load->view('landing/_partials/head'); ?>

</head>

<body>

    <?php $this->load->view('landing/_partials/navbar'); ?>

    <!-- home start -->
    <section class="bg-home bg-gradient" id="home">
        <div class="home-center">
            <div class="home-desc-center">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-lg-6">
                            <div class="home-title mo-mb-20">
                                <h1 class="mb-4 text-white">SIMalas</h1>
                                <p class="text-white-50 home-desc mb-5">Sebuah aplikasi untuk memanage kelas dari mulai sistem kas sampai dengan absensi dan banyak lagi lainnya. </p>
                                <div class="subscribe">
                                    <form action="javascript:void(0)" method="post">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="mb-2">
                                                    <input type="text" name="email_subscribe" id="inputSubscribe" class="form-control" placeholder="Masukkan e-mail anda">
                                                    <span class="help-block text-white">
                                                        <small id="response_subscribe"></small>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <button type="submit" id="btn_email_subscribe" class="btn btn-primary" onclick="getSubscribe();">Dapatkan informasi</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 offset-xl-2 col-lg-5 offset-lg-1 col-md-7">
                            <div class="home-img position-relative">
                                <img src="<?php echo base_url('assets_landing'); ?>/images/home-img.png" alt="" class="home-first-img">
                                <img src="<?php echo base_url('assets_landing'); ?>/images/home-img.png" alt="" class="home-second-img mx-auto d-block">
                                <img src="<?php echo base_url('assets_landing'); ?>/images/home-img.png" alt="" class="home-third-img">
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                </div>
                <!-- end container-fluid -->
            </div>
        </div>
    </section>
    <!-- home end -->

    <!-- clients start -->
    <section>
        <div class="container-fluid">
            <div class="clients p-4 bg-white">
                <div class="row">
                    <div class="col-md-3">
                        <div class="client-images">
                            <img src="<?php echo base_url('assets_landing'); ?>/images/clients/1.png" alt="logo-img" class="mx-auto img-fluid d-block">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="client-images">
                            <img src="<?php echo base_url('assets_landing'); ?>/images/clients/3.png" alt="logo-img" class="mx-auto img-fluid d-block">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="client-images">
                            <img src="<?php echo base_url('assets_landing'); ?>/images/clients/4.png" alt="logo-img" class="mx-auto img-fluid d-block">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="client-images">
                            <img src="<?php echo base_url('assets_landing'); ?>/images/clients/6.png" alt="logo-img" class="mx-auto img-fluid d-block">
                        </div>
                    </div>
                </div> <!-- end row -->
            </div>
        </div> <!-- end container-fluid -->
    </section>
    <!-- clients end -->

    <!-- features start -->
    <section class="section-sm" id="fitur">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="text-center mb-4 pb-1">
                        <h3 class="mb-3">The admin is fully responsive and easy to customize</h3>
                        <p class="text-muted">The clean and well commented code allows easy customization of the theme.It's designed for describing your app, agency or business.</p>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-lg-4">
                    <div class="features-box">
                        <div class="features-img mb-4">
                            <img src="<?php echo base_url('assets_landing'); ?>/images/icons/layers.png" alt="">
                        </div>
                        <h4 class="mb-2">Responsive Layouts</h4>
                        <p class="text-muted">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit</p>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-lg-4">
                    <div class="features-box">
                        <div class="features-img mb-4">
                            <img src="<?php echo base_url('assets_landing'); ?>/images/icons/core.png" alt="">
                        </div>
                        <h4 class="mb-2">Based on Bootstrap UI</h4>
                        <p class="text-muted">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium</p>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-lg-4">
                    <div class="features-box">
                        <div class="features-img mb-4">
                            <img src="<?php echo base_url('assets_landing'); ?>/images/icons/paperdesk.png" alt="">
                        </div>
                        <h4 class="mb-2">Creative Design</h4>
                        <p class="text-muted">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p>
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->

            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <div class="features-box">
                        <div class="features-img mb-4">
                            <img src="<?php echo base_url('assets_landing'); ?>/images/icons/solarsystem.png" alt="">
                        </div>
                        <h4 class="mb-3">Awesome Support</h4>
                        <p class="text-muted">At solmen va esser necessi far uniform grammatica pronun e plu sommun paroles.</p>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-lg-4">
                    <div class="features-box">
                        <div class="features-img mb-4">
                            <img src="<?php echo base_url('assets_landing'); ?>/images/icons/datatext.png" alt="">
                        </div>
                        <h4 class="mb-3">Easy to customize</h4>
                        <p class="text-muted">If several languages coalesce the grammar of the is more simple languages.</p>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-lg-4">
                    <div class="features-box">
                        <div class="features-img mb-4">
                            <img src="<?php echo base_url('assets_landing'); ?>/images/icons/browserscript.png" alt="">
                        </div>
                        <h4 class="mb-3">Quality Code</h4>
                        <p class="text-muted">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p>
                    </div>
                </div>
                <!-- end col -->
            </div> <!-- end row -->
        </div> <!-- end container-fluid -->
    </section>
    <!-- features end -->

    <!-- available demos start -->
    <section class="section bg-light" id="demo">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title text-center mb-3">
                        <h3>Available Demos</h3>
                        <p class="text-muted">At solmen va esser necessi far uniform grammatica.</p>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="demo-box bg-white mt-4 p-2">
                        <a href="#" class="text-dark">
                            <img src="<?php echo base_url('assets_landing'); ?>/images/demo/index_1.jpg" alt="" class="img-fluid mx-auto d-block">
                            <div class="p-3 text-center">
                                <h5 class="mb-0">light Layouts</h5>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-lg-4 col-md-6">
                    <div class="demo-box bg-white mt-4 p-2">
                        <a href="#" class="text-dark">
                            <img src="<?php echo base_url('assets_landing'); ?>/images/demo/index_2.jpg" alt="" class="img-fluid mx-auto d-block">
                            <div class="p-3 text-center">
                                <h5 class="mb-0">Dark Layouts</h5>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-lg-4 col-md-6">
                    <div class="demo-box bg-white mt-4 p-2">
                        <a href="#" class="text-dark">
                            <img src="<?php echo base_url('assets_landing'); ?>/images/demo/index_3.jpg" alt="" class="img-fluid mx-auto d-block">
                            <div class="p-3 text-center">
                                <h5 class="mb-0">Material Design </h5>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-lg-4 col-md-6">
                    <div class="demo-box bg-white mt-4 p-2">
                        <a href="#" class="text-dark">
                            <img src="<?php echo base_url('assets_landing'); ?>/images/demo/index_4.jpg" alt="" class="img-fluid mx-auto d-block">
                            <div class="p-3 text-center">
                                <h5 class="mb-0">Purple Layouts</h5>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-lg-4 col-md-6">
                    <div class="demo-box bg-white mt-4 p-2">
                        <a href="#" class="text-dark">
                            <img src="<?php echo base_url('assets_landing'); ?>/images/demo/index_5.jpg" alt="" class="img-fluid mx-auto d-block">
                            <div class="p-3 text-center">
                                <h5 class="mb-0">Boxed Layouts</h5>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-lg-4 col-md-6">
                    <div class="demo-box bg-white mt-4 p-2">
                        <a href="#" class="text-dark">
                            <img src="<?php echo base_url('assets_landing'); ?>/images/demo/index_6.jpg" alt="" class="img-fluid mx-auto d-block">
                            <div class="p-3 text-center">
                                <h5 class="mb-0">Horizontal </h5>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-12">
                    <div class="text-center mt-4">
                        <button class="btn btn-info navbar-btn">More Demos <i class="mdi mdi-arrow-right"></i></button>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div> <!-- end container-fluid -->
    </section>
    <!-- available demos end -->

    <!-- features start -->
    <section class="section">
        <div class="container-fluid">

            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="text-center mb-4 pb-1">
                        <h3>Ultra Features</h3>
                        <p class="text-muted">The clean and well commented code allows easy customization of the theme.It's designed for describing your app, agency or business.</p>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="feature-img">
                        <img src="<?php echo base_url('assets_landing'); ?>/images/features-img/img-2.png" alt="" class="img-fluid mx-auto d-block">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="p-5 features-content">
                        <div class="features-icon mb-4">
                            <i class="mdi mdi-chart-bar h4 text-white text-center"></i>
                        </div>
                        <h3 class="mb-3">It's designed for describing your app, agency or business</h3>
                        <p class="text-muted mb-4">
                            If several languages coalesce the grammar of the resulting language is more simple and regular than that of the individual languages.
                        </p>
                        <a href="#" class="btn btn-primary btn-sm">Learn More <i class="mdi mdi-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row align-items-center mt-5">
                <div class="col-lg-6">
                    <div class="p-5 features-content">
                        <div class="features-icon mb-4">
                            <i class="mdi mdi-auto-fix h4 text-white text-center"></i>
                        </div>
                        <h3 class="mb-3">Explore the new world of creativity</h3>
                        <p class="text-muted mb-4">
                            Everyone realizes why a new common language would be desirable one could refuse to expensive translators it would be necessary.
                        </p>
                        <a href="#" class="btn btn-primary btn-sm">Learn More <i class="mdi mdi-arrow-right"></i></a>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="feature-img">
                        <img src="<?php echo base_url('assets_landing'); ?>/images/features-img/img-1.png" alt="" class="img-fluid mx-auto d-block">
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </section>
    <!-- features end -->

    <!-- pricing start -->
    <section class="section pb-0 bg-gradient" id="pricing">
        <div class="bg-shape">
            <img src="<?php echo base_url('assets_landing'); ?>/images/bg-shape.png" alt="" class="img-fluid mx-auto d-block">
        </div>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="text-center mb-4">
                        <h3 class="text-white">Choose Our Pricing Plans</h3>
                        <p class="text-white-50">The clean and well commented code allows easy customization of the theme.It's designed for describing your app, agency or business.</p>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row justify-content-center">
                <div class="col-xl-10">
                    <div class="row align-items-center">
                        <div class="col-lg-4">
                            <div class="pricing-plan bg-white p-4 mt-4">
                                <div class="pricing-header text-center">
                                    <h5 class="plan-title text-uppercase mb-4">Single Application</h5>
                                    <h1><sup><small>$</small></sup>24</h1>
                                    <div class="plan-duration text-muted">Per License</div>
                                </div>
                                <ul class="list-unstyled pricing-list mt-4">
                                    <li>
                                        <i class="mdi mdi-album"></i>
                                        <p>Number of end products <b>1</b></p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-lifebuoy"></i>
                                        <p>Customer support<p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-update"></i>
                                        <p>Free Updates</p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-earth"></i>
                                        <p> 1 Domain<p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-history"></i>
                                        <p>Monthly updates</p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-alarm-check"></i>
                                        <p>24x7 Support</p>
                                    </li>
                                </ul>
                                <div class="text-center mt-5">
                                    <a href="#" class="btn btn-primary">Purchase Now</a>
                                </div>
                            </div>
                        </div> <!-- end col -->

                        <div class="col-lg-4">
                            <div class="pricing-plan active p-4 mt-4">
                                <span class="lable">Popular</span>
                                <div class="pricing-header text-white text-center">
                                    <h5 class="plan-title text-white text-uppercase mb-4">Multiple Application</h5>
                                    <h1 class=" text-white"><sup><small>$</small></sup>120</h1>
                                    <div class="plan-duration">Per License</div>
                                </div>
                                <ul class="list-unstyled text-white pricing-list mt-4">
                                    <li>
                                        <i class="mdi mdi-album"></i>
                                        <p>Number of end products <b>1</b></p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-lifebuoy"></i>
                                        <p>Customer support<p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-update"></i>
                                        <p>Free Updates</p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-earth"></i>
                                        <p> 1 Domain<p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-history"></i>
                                        <p>Monthly updates</p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-alarm-check"></i>
                                        <p>24x7 Support</p>
                                    </li>
                                </ul>
                                <div class="text-center mt-5">
                                    <a href="#" class="btn shadow btn-light">Purchase Now</a>
                                </div>
                            </div>
                        </div> <!-- end col -->

                        <div class="col-lg-4">
                            <div class="pricing-plan bg-white p-4 mt-4">
                                <div class="pricing-header text-center">
                                    <h5 class="plan-title text-uppercase mb-4">Extended</h5>
                                    <h1><sup><small>$</small></sup>999</h1>
                                    <div class="plan-duration text-muted">Per License</div>
                                </div>
                                <ul class="list-unstyled pricing-list mt-4">
                                    <li>
                                        <i class="mdi mdi-album"></i>
                                        <p>Number of end products <b>1</b></p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-lifebuoy"></i>
                                        <p>Customer support<p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-update"></i>
                                        <p>Free Updates</p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-earth"></i>
                                        <p> 1 Domain<p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-history"></i>
                                        <p>Monthly updates</p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-alarm-check"></i>
                                        <p>24x7 Support</p>
                                    </li>
                                </ul>
                                <div class="text-center mt-5">
                                    <a href="#" class="btn btn-primary">Purchase Now</a>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div>
                    <!-- end row -->
                </div> <!-- end col-xl-10 -->

            </div>
            <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </section>
    <!-- pricing end -->

    <!-- faqs start -->
    <section class="section" id="pertanyaan">
        <div class="container-fluid">

            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="text-center mb-5">
                        <h3>Pertanyaan yang Sering Diajukan</h3>
                        <p class="text-muted">Pada khidmat itu perlu untuk membuat tata bahasa yang seragam.</p>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-lg-5 offset-lg-1">
                    <!-- Question/Answer -->
                    <div>
                        <div class="faq-question-q-box">Q.</div>
                        <h4 class="faq-question">Bagaimana cara mendaftarnya?</h4>
                        <p class="faq-answer mb-4 pb-1 text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>

                    <!-- Question/Answer -->
                    <div>
                        <div class="faq-question-q-box">Q.</div>
                        <h4 class="faq-question">Apa kelebihan dari aplikasi ini?</h4>
                        <p class="faq-answer mb-4 pb-1 text-muted">Lorem ipsum dolor sit amet, in mea nonumes dissentias dissentiunt, pro te solet oratio iriure. Cu sit consetetur moderatius intellegam, ius decore accusamus te. Ne primis suavitate disputando nam. Mutat convenirete.</p>
                    </div>

                    <!-- Question/Answer -->
                    <div>
                        <div class="faq-question-q-box">Q.</div>
                        <h4 class="faq-question">Kenapa kita harus memakai aplikasi ini?</h4>
                        <p class="faq-answer mb-4 pb-1 text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>

                </div>
                <!--/col-lg-5 -->

                <div class="col-lg-5">
                    <!-- Question/Answer -->
                    <div>
                        <div class="faq-question-q-box">Q.</div>
                        <h4 class="faq-question">Kekurangan</h4>
                        <p class="faq-answer mb-4 pb-1 text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>

                    <!-- Question/Answer -->
                    <div>
                        <div class="faq-question-q-box">Q.</div>
                        <h4 class="faq-question">When can be used?</h4>
                        <p class="faq-answer mb-4 pb-1 text-muted">Lorem ipsum dolor sit amet, in mea nonumes dissentias dissentiunt, pro te solet oratio iriure. Cu sit consetetur moderatius intellegam, ius decore accusamus te. Ne primis suavitate disputando nam. Mutat convenirete.</p>
                    </div>

                    <!-- Question/Answer -->
                    <div>
                        <div class="faq-question-q-box">Q.</div>
                        <h4 class="faq-question">License &amp; Copyright</h4>
                        <p class="faq-answer mb-4 pb-1 text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>

                </div>
                <!--/col-lg-5-->
            </div>
            <!-- end row -->

        </div> <!-- end container-fluid -->
    </section>
    <!-- faqs end -->

    <!-- testimonial start -->
    <section class="section bg-light" id="testimoni">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="text-center mb-4">
                        <h3>Apa Kata Pengguna kami</h3>
                        <p class="text-muted">Sistem yang mudah dipelajari dan tidak terlalu banyak sistem yang bertindihan.</p>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-lg-4">
                    <div class="testi-box mt-4">
                        <div class="testi-desc bg-white p-4">
                            <p class="text-muted mb-0">" Dukungan luar biasa untuk masalah rumit terkait dengan penyesuaian templat kami. Penulis terus memberi informasi kepada kami saat ia membuat kemajuan tentang masalah ini dan mengirimi kami patch saat ia selesai. "</p>
                        </div>
                        <div class="p-4">
                            <div class="testi-img float-left mr-2">
                                <img src="<?php echo base_url('assets_landing'); ?>/images/testi/img-2.png" alt="" class="rounded-circle">
                            </div>
                            <div>
                                <h5 class="mb-0">Rio Prastiawan</h5>
                                <p class="text-muted m-0"><small>- SMK N 2 Semarang</small></p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-lg-4">
                    <div class="testi-box mt-4">
                        <div class="testi-desc bg-white p-4">
                            <p class="text-muted mb-0">" Fleksibel, Semuanya ada, cahaya Suuuuuper, bahkan untuk kode jauh lebih mudah untuk memotong dan menjadikannya tema untuk aplikasi yang produktif. "</p>
                        </div>
                        <div class="p-4">
                            <div class="testi-img float-left mr-2">
                                <img src="<?php echo base_url('assets_landing'); ?>/images/testi/img-1.png" alt="" class="rounded-circle">
                            </div>
                            <div>
                                <h5 class="mb-0">Bimo Rio</h5>
                                <p class="text-muted m-0"><small>- SMK N 2 Semarang</small></p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-lg-4">
                    <div class="testi-box mt-4">
                        <div class="testi-desc bg-white p-4">
                            <p class="text-muted mb-0">" Tidak hanya kode, desain, dan dukungan yang luar biasa, tetapi mereka juga memperbaruinya secara konstan dengan konten baru, plugin baru. Saya pasti akan membeli template coderthemes lain! "</p>
                        </div>
                        <div class="p-4">
                            <div class="testi-img float-left mr-2">
                                <img src="<?php echo base_url('assets_landing'); ?>/images/testi/img-3.png" alt="" class="rounded-circle">
                            </div>
                            <div>
                                <h5 class="mb-0">Prastiawan Rio</h5>
                                <p class="text-muted m-0"><small>- SMK N 2 Semarang</small></p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end col -->
            </div> 
            <!-- end row -->
        </div> 
        <!-- end container-fluid -->
    </section>
    <!-- testimonial end -->

    <!-- contact start -->
    <section class="section pb-0 bg-gradient" id="kontak">
        <div class="bg-shape">
            <img src="<?php echo base_url('assets_landing'); ?>/images/bg-shape-light.png" alt="" class="img-fluid mx-auto d-block">
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title text-center mb-4">
                        <h3 class="text-white">Ada pertanyaan ?</h3>
                        <p class="text-white-50">Silakan isi formulir berikut dan kami akan segera menghubungi Anda</p>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row mb-4">
                <div class="col-lg-4">
                    <div class="contact-content text-center mt-4">
                        <div class="contact-icon mb-2">
                            <i class="mdi mdi-email-outline text-info h2"></i>
                        </div>
                        <div class="contact-details text-white">
                            <h5 class="text-white">Surel</h5>
                            <a href="mailto:akunviprio@gmail.com"><p class="text-white-50">akunviprio@gmail.com</p></a>
                        </div>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-lg-4">
                    <div class="contact-content text-center mt-4">
                        <div class="contact-icon mb-2">
                            <i class="mdi mdi-cellphone-iphone text-info h2"></i>
                        </div>
                        <div class="contact-details">
                            <h5 class="text-white">No HP</h5>
                            <a href="http://wa.me/628990125338" target="_blank" rel="noopener noreferrer"><p class="text-white-50">0899-0125-338</p></a>
                        </div>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-lg-4">
                    <div class="contact-content text-center mt-4">
                        <div class="contact-icon mb-2">
                            <i class="mdi mdi-map-marker text-info h2"></i>
                        </div>
                        <div class="contact-details">
                            <h5 class="text-white">Alamat</h5>
                            <a href="http://bit.ly/2NfD1zj" target="_blank" rel="noopener noreferrer"><p class="text-white-50">34 Tirtoyoso Tengah, Kota Semarang</p></a>
                        </div>
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->

            <div class="row justify-content-center">
                <div class="col-lg-10">

                    <div class="custom-form p-5 bg-white">
                        <div id="message"></div>
                        <form method="post" action="javascript:void(0)" name="contact-form" id="contact-form">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="nama_pertanyaan">Nama</label>
                                        <input name="nama_pertanyaan" id="nama_pertanyaan" type="text" class="form-control" placeholder="Masukkan nama Anda...">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="email_pertanyaan">Email</label>
                                        <input name="email_pertanyaan" id="email_pertanyaan" type="email" class="form-control" placeholder="Masukkan email Anda...">
                                    </div>
                                </div>
                            </div>
                            <!-- end row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="judul_pertanyaan">Judul</label>
                                        <input name="judul_pertanyaan" id="judul_pertanyaan" type="text" class="form-control" placeholder="Masukkan judul...">
                                    </div>
                                </div>
                            </div>
                            <!-- end row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="pesan_pertanyaan">Pesan</label>
                                        <textarea name="pesan_pertanyaan" id="pesan_pertanyaan" rows="4" class="form-control" placeholder="Masukkan pesan Anda..."></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- end row -->

                            <div class="row">
                                <div class="col-lg-6 text-left">
                                    <div id="simple-msg" style="margin-top:10px;"></div>
                                </div>
                                <div class="col-lg-6 text-right">
                                    <input type="submit" id="submit_pertanyaan" name="kirim_pertanyaan" class="submitBnt btn btn-danger" value="Kirim Pesan" onclick="getPertanyaan();">
                                </div>
                            </div>
                            <!-- end row -->
                        </form>
                    </div>
                    <!-- end custom-form -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </section>
    <!-- contact end -->

    <!-- cta start -->
    <section class="section-sm bg-light">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-9">
                    <h3 class="mb-0 mo-mb-20">Kami juga menyesuaikan tema sesuai kebutuhan Anda</h3>
                </div>
                <div class="col-md-3">
                    <div class="text-md-right">
                        <a href="#kontak" class="btn btn-outline-dark btn-rounded"><i class="mdi mdi-email-outline mr-1"></i> Hubungi Kami</a>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </section>
    <!-- cta end -->

    <?php $this->load->view('landing/_partials/footer'); ?>
    
    <?php $this->load->view('landing/_partials/back_to_top'); ?>

    <?php $this->load->view('landing/_partials/js'); ?>

    <script>
    
        function getSubscribe(){
            var email = $('#inputSubscribe').val();

            $('#inputSubscribe').attr('disabled','');
            $('#btn_email_subscribe').attr('disabled','');

            $.ajax({
                type:'POST',
                data: 'email_subscribe='+email,
                url: 'proses/tambah-subscribe',
                success: function(result){
                    var response = JSON.parse(result);

                    if(response.status == 'sukses'){
                        $('#inputSubscribe').val('');    
                    }
                    $('#response_subscribe').html(response.icon +response.status + " - " + response.pesan);

                    $('#inputSubscribe').removeAttr('disabled','');
                    $('#btn_email_subscribe').removeAttr('disabled','');
                }
            })
        }

        function getPertanyaan(){
            var nama = $('#nama_pertanyaan').val();
            var email = $('#email_pertanyaan').val();
            var judul = $('#judul_pertanyaan').val();
            var isi = $('#pesan_pertanyaan').val();

            $('#nama_pertanyaan').attr('disabled','');
            $('#email_pertanyaan').attr('disabled','');
            $('#judul_pertanyaan').attr('disabled','');
            $('#pesan_pertanyaan').attr('disabled','');
            $('#submit_pertanyaan').attr('disabled','');
            
            $.ajax({
                type:'POST',
                data: 'nama_pertanyaan='+nama+'&email_pertanyaan='+email+'&judul_pertanyaan='+judul+'&pesan_pertanyaan='+isi,
                url: 'proses/tambah-pertanyaan',
                success: function(result){
                    var response = JSON.parse(result);

                    if(response.status == 'sukses'){
                        $('#nama_pertanyaan').val('');
                        $('#email_pertanyaan').val('');
                        $('#judul_pertanyaan').val('');
                        $('#pesan_pertanyaan').val('');
                    }
                    $('#simple-msg').html(response.icon +response.status + " - " + response.pesan);
                    
                    $('#nama_pertanyaan').removeAttr('disabled','');
                    $('#email_pertanyaan').removeAttr('disabled','');
                    $('#judul_pertanyaan').removeAttr('disabled','');
                    $('#pesan_pertanyaan').removeAttr('disabled','');
                    $('#submit_pertanyaan').removeAttr('disabled','');
                }
            })
        }
    
    </script>
</body>
</html>