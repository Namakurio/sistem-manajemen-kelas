<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">

    <div class="slimscroll-menu">

        <!-- User box -->
        <div class="user-box text-center">
            <img src="<?php echo base_url(); ?>upload/profile/user.jpg" alt="user-img" title="Mat Helme" class="rounded-circle avatar-md">
            <div class="dropdown">
                <a href="javascript: void(0);" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block" data-toggle="dropdown"><?php echo $this->session->userdata('nama_user'); ?></a>
                <div class="dropdown-menu user-pro-dropdown">

                    <!-- item-->
                    <a href="<?php echo site_url('profile'); ?>" class="dropdown-item notify-item">
                        <i class="fe-user mr-1"></i>
                        <span>Profil</span>
                    </a>

                    <!-- item-->
                    <a href="<?php echo site_url('logout'); ?>" class="dropdown-item notify-item">
                        <i class="fe-log-out mr-1"></i>
                        <span>Keluar</span>
                    </a>

                </div>
            </div>
            <p class="text-muted"><?php echo $this->session->userdata('nama_level'); ?></p>
        </div>

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                <li class="menu-title">Menu</li>

                <li>
                    <a href="<?php echo site_url('dashboard'); ?>">
                        <i class="fe-home"></i>
                        <span> Dashboard </span>
                    </a>
                </li>

                <li>
                    <a href="<?php echo site_url('profile'); ?>">
                        <i class="fe-user"></i>
                        <span> Profile </span>
                    </a>
                </li>

                <li class="menu-title"><?php echo $this->session->userdata('nama_level'); ?></li>

                <li>
                    <a href="<?php echo site_url('institusi'); ?>">
                        <i class="mdi mdi-bank"></i>
                        <span> Sekolah</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo site_url('users'); ?>">
                        <i class="fe-users"></i>
                        <span> Users </span>
                    </a>
                </li>
                
                <li>
                    <a href="<?php echo site_url('pengaturan'); ?>">
                        <i class="fe-settings"></i>
                        <span> Pengaturan </span>
                    </a>
                </li>
                <li class="menu-title">Lainnya</li>
                <li>
                    <a href="<?php echo site_url('faq'); ?>">
                        <i class="fe-user-check"></i>
                        <span> Pertanyaan</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo site_url('testimoni'); ?>">
                        <i class="mdi mdi-bookmark-check"></i>
                        <span> Testimoni </span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo site_url('log'); ?>">
                        <i class="mdi mdi-server-network"></i>
                        <span> Log </span>
                    </a>
                </li>
            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
        <!-- Left Sidebar End -->