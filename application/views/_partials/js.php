<!-- Vendor js -->
<script src="<?php echo base_url(); ?>assets/js/vendor.min.js"></script>

<!-- third party js -->
<script src="<?php echo base_url(); ?>assets/libs/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/datatables/dataTables.bootstrap4.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/datatables/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/datatables/buttons.flash.min.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/datatables/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/datatables/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/datatables/dataTables.select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/libs/pdfmake/vfs_fonts.js"></script>
<!-- third party js ends -->

<!-- Magnific Popup-->
<script src="<?php echo base_url(); ?>assets/libs/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Gallery Init-->
<script src="<?php echo base_url(); ?>assets/js/pages/gallery.init.js"></script>

<!-- Plugin js-->
<script src="<?php echo base_url(); ?>assets/libs/parsleyjs/parsley.min.js"></script>

<!-- Validation init js-->
<script src="<?php echo base_url(); ?>assets/js/pages/form-validation.init.js"></script>
<!-- Datatables init -->
<script src="<?php echo base_url(); ?>assets/js/pages/datatables.init.js"></script>
<!-- Tippy js-->
<script src="<?php echo base_url(); ?>assets/libs/tippy-js/tippy.all.min.js"></script>

<!-- App js -->
<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>