<!-- New Users Modal-->
<div class="modal fade" id="jawabPertanyaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Jawab Pertanyaan</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<form action="javascript:void(0)" method="post">
				<div class="modal-body">
					<div class="form-group mb-3">
						<label for="lbl_nama">Nama</label>
						<input type="text" id="nama_faq" name="nama_faq" class="form-control" disabled>
					</div>
					<div class="form-group mb-3">
						<label for="lbl_email">Email</label>
						<input type="email" id="email_faq" name="email_faq" class="form-control" disabled>
					</div>
					<div class="form-group mb-3">
						<label for="lbl_username">Judul</label>
						<input type="text" id="judul_faq" name="judul_faq" class="form-control" disabled>
					</div>
					<div class="form-group mb-3">
						<label for="lbl_email">Soal</label>
						<textarea class="form-control" id="soal_faq" name="soal_faq" rows="5"disabled></textarea>
					</div>
					<div class="form-group mb-3">
						<label for="lbl_email">Jawaban</label>
						<textarea class="form-control" id="jawaban_faq" name="jawaban_faq" rows="5" placeholder="Masukkan Jawaban Anda.."></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
					<button type="submit" id="kirim_jawaban" class="btn btn-success">Simpan Jawaban</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- Logout Delete Confirmation-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
				<a id="btn-delete" class="btn btn-danger" href="#">Delete</a>
			</div>
		</div>
	</div>
</div>