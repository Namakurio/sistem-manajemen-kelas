<?php if(isset($_SESSION['gagal'])){ ?>
<div class="alert alert-primary alert-dismissible bg-danger text-white border-0 fade show" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <?php echo $_SESSION['gagal']; ?>
</div>
<?php }else if(isset($_SESSION['sukses'])){ ?>
    <div class="alert alert-primary alert-dismissible bg-success text-white border-0 fade show" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <?php echo $_SESSION['sukses']; ?>
</div>
<?php } ?>